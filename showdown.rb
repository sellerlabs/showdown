#!/usr/bin/ruby
require 'socket'
require 'uptimerobot'
require 'time'
client = UptimeRobot::Client.new(apiKey: ENV['UPTIMEROBOT_KEY'])
server = TCPServer.new('0.0.0.0', ENV['PORT'])
loop do
  socket = server.accept
  request = socket.gets
  STDERR.puts request
  monnames = []
  # only query bad services at the beginning of the hour.  This is a terrible hack.
  client.getMonitors(statuses:9)['monitors']['monitor'].each {|m| monnames.push(m['friendlyname'])} if (Time.now.min < 10 and request =~ /reminder=true/) or request !~ /reminder=true/
  if monnames.size == 0
    monnames = ['ok', 'all systems go']
    status = "200 OK"
  else
    status = "503 Some Services Are Down"
  end
  STDERR.puts monnames
  response = monnames.join(",")
  socket.print "HTTP/1.1 #{status}\r\n" +
               "Content-Type: text/plain\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"
  socket.print "\r\n"
  socket.print response
  socket.close
end
