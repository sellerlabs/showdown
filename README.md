Build a docker container that listens on a given port, queries uptimerobot (given an API key), and lists any services marked as down.

The whole reason that this is needed is because uptimerobot.com does not have a reminder feature.

So, we'll set up an uptimerobot monitor to query this container, which really queries itself, and alerts every X if any service is down.  Yes, this is an ugly hack.
