FROM ruby
RUN gem install uptimerobot
COPY showdown.rb /
CMD ["ruby","/showdown.rb"]
